import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ReactionType } from '../enums/reactionType.enum';

export type ReactionDocument = Reaction & Document;

@Schema()
export class Reaction {
  @Prop()
  targetId!: string;

  @Prop({ enum: ReactionType })
  type!: ReactionType;

  @Prop()
  ownerId!: string;
}

export const ReactionSchema = SchemaFactory.createForClass(Reaction);
ReactionSchema.index({ targetId: 1, ownerId: 1 }, { unique: true });
