import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { iif, map, mergeMap, Observable, of, tap, throwError } from 'rxjs';
import { DeleteReactionRequestDto } from './dtos/deleteReactionRequest.dto';
import { GetReactionsForTargetRequestDto } from './dtos/getReactionsForTargetRequest.dto';
import { ReactionForCreationDto } from './dtos/reactionForCreation.dto';
import { ReactionForUpdateDto } from './dtos/reactionForUpdate.dto';
import { UpdateReactionRequestDto } from './dtos/updateReactionRequest.dto';
import { GetReactionsForTargetRequest } from './interfaces/getReactionsForTargetRequest.interface';
import { GetReactionsForTargetResponse } from './interfaces/getReactionsForTargetResponse.interface';
import { ReactionsService } from './reactions.service';
import { ReactionDocument } from './schemas/reaction.schema';

@Controller('reactions')
export class ReactionsController {
  constructor(private readonly _reactionsService: ReactionsService) {}

  @Get()
  get(@Query() requestDto: GetReactionsForTargetRequestDto) {
    return this._reactionsService.findAllForTarget(requestDto.targetId);
  }

  @Post()
  add(@Body() creationDto: ReactionForCreationDto) {
    return this._reactionsService.addToTarget(creationDto);
  }

  @Patch()
  update(
    @Query() requestDto: UpdateReactionRequestDto,
    @Body() updateDto: ReactionForUpdateDto,
  ) {
    const { targetId, ownerId } = requestDto;

    return this.findByOwnerAndTarget(targetId, ownerId).pipe(
      mergeMap((reaction) =>
        this._reactionsService.update(reaction!.id, updateDto),
      ),
    );
  }

  @Delete()
  @HttpCode(HttpStatus.NO_CONTENT)
  delete(@Query() requestDto: DeleteReactionRequestDto) {
    const { targetId, ownerId } = requestDto;

    return this.findByOwnerAndTarget(targetId, ownerId).pipe(
      mergeMap((reaction) => this._reactionsService.delete(reaction!.id)),
    );
  }

  @GrpcMethod('ReactionsService')
  getAllForTarget(
    request: GetReactionsForTargetRequest,
  ): Observable<GetReactionsForTargetResponse> {
    return this._reactionsService
      .findAllForTarget(request.id)
      .pipe(map((reactions) => ({ reactions })));
  }

  private findByOwnerAndTarget(targetId: string, ownerId: string) {
    return this._reactionsService.findByTargetAndOwner(targetId, ownerId).pipe(
      mergeMap((reaction) =>
        iif(
          () => !!reaction,
          of(reaction),
          throwError(() => new NotFoundException('Reaction cannot be found')),
        ),
      ),
    );
  }
}
