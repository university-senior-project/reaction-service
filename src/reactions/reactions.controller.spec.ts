import { Test } from '@nestjs/testing';
import { firstValueFrom, of } from 'rxjs';
import { GetReactionsForTargetRequestDto } from './dtos/getReactionsForTargetRequest.dto';
import { ReactionForCreationDto } from './dtos/reactionForCreation.dto';
import { ReactionForUpdateDto } from './dtos/reactionForUpdate.dto';
import { UpdateReactionRequestDto } from './dtos/updateReactionRequest.dto';
import { ReactionsController } from './reactions.controller';
import { ReactionsService } from './reactions.service';
import { NotFoundException } from '@nestjs/common';
import { DeleteReactionRequestDto } from './dtos/deleteReactionRequest.dto';

const reactions = [
  {
    _id: '618920a4c9bab5a94ffc20b1',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 1,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920a7c9bab5a94ffc20b4',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920a9c9bab5a94ffc20b7',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920aac9bab5a94ffc20ba',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920aac9bab5a94ffc20bd',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920abc9bab5a94ffc20c0',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920abc9bab5a94ffc20c3',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920acc9bab5a94ffc20c6',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920adc9bab5a94ffc20c9',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920aec9bab5a94ffc20cc',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920aec9bab5a94ffc20cf',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618920afc9bab5a94ffc20d2',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618926f0e0faa6bbd5552aba',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618926f4e0faa6bbd5552abe',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 1,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '618926f8e0faa6bbd5552ac3',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 0,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
  {
    _id: '61892707e0faa6bbd5552ac9',
    ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
    type: 1,
    targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
    __v: 0,
  },
];

describe('ReactionsController unit tests', () => {
  let reactionsController: ReactionsController;

  const mockReactionsService = {
    findAllForTarget: jest.fn().mockReturnValue(of(reactions)),
    findByTargetAndOwner: jest.fn().mockReturnValue(of(reactions[0])),
    addToTarget: jest.fn().mockReturnValue(of(reactions[0])),
    findById: jest.fn().mockReturnValue(of(reactions[0])),
    update: jest.fn().mockReturnValue(of(reactions[0])),
    delete: jest.fn().mockReturnValue(of(reactions[0])),
  };

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ReactionsController],
      providers: [
        {
          provide: ReactionsService,
          useValue: mockReactionsService,
        },
      ],
    }).compile();

    reactionsController =
      moduleRef.get<ReactionsController>(ReactionsController);
  });

  describe('get all reactions for target', () => {
    it('should call service function and return list of reactions', async () => {
      const dto: GetReactionsForTargetRequestDto = { targetId: 'some_id' };

      const result = await firstValueFrom(reactionsController.get(dto));

      expect(mockReactionsService.findAllForTarget).toBeCalled();
      expect(result).toStrictEqual(reactions);
    });
  });

  describe('add reaction to target', () => {
    it('should call service function and return added reaction', async () => {
      const dto: ReactionForCreationDto = {
        targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
        type: 0,
        ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
      };

      const result = await firstValueFrom(reactionsController.add(dto));

      expect(mockReactionsService.addToTarget).toBeCalled();
      expect(result).toBe(reactions[0]);
    });
  });

  describe('update reaction', () => {
    it('should call service and return updated reaction', async () => {
      const requestDto: UpdateReactionRequestDto = {
        targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
        ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
      };

      const updateDto: ReactionForUpdateDto = { type: 0 };

      const result = await firstValueFrom(
        reactionsController.update(requestDto, updateDto),
      );

      expect(mockReactionsService.update).toBeCalled();
      expect(result).toBe(reactions[0]);
    });

    it('should throw not found exception when updating inexistent reaction', async () => {
      mockReactionsService.findByTargetAndOwner.mockReturnValue(of(null));

      try {
        const requestDto: UpdateReactionRequestDto = {
          targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
          ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
        };

        const updateDto: ReactionForUpdateDto = { type: 0 };

        const _ = await firstValueFrom(
          reactionsController.update(requestDto, updateDto),
        );
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
      }
    });
  });

  describe('delete reaction ', () => {
    it('should call service and return deleted reaction', async () => {
      mockReactionsService.findByTargetAndOwner.mockReturnValue(
        of(reactions[0]),
      );

      const requestDto: DeleteReactionRequestDto = {
        targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
        ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
      };

      const result = await firstValueFrom(
        reactionsController.delete(requestDto),
      );

      expect(result).toBe(reactions[0]);
    });

    it('should throw not found exception', async () => {
      mockReactionsService.findByTargetAndOwner.mockReturnValue(of(null));

      try {
        const requestDto: DeleteReactionRequestDto = {
          targetId: '6a73550d-d05a-46e1-a94b-4279a4611f96',
          ownerId: '368460ec-f93c-4193-b13b-936bb479a5ed',
        };

        const _ = await firstValueFrom(reactionsController.delete(requestDto));
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
      }
    });
  });
});
