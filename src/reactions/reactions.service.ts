import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from, iif, mergeMap, of } from 'rxjs';
import { ReactionForCreationDto } from './dtos/reactionForCreation.dto';
import { ReactionForUpdateDto } from './dtos/reactionForUpdate.dto';
import { Reaction, ReactionDocument } from './schemas/reaction.schema';

@Injectable()
export class ReactionsService {
  constructor(
    @InjectModel(Reaction.name)
    private readonly _reactionModel: Model<ReactionDocument>,
  ) {}

  findAllForTarget(targetId: string) {
    return from(this._reactionModel.find({ targetId }).exec());
  }

  findByTargetAndOwner(targetId: string, ownerId: string) {
    return from(this._reactionModel.findOne({ targetId, ownerId }));
  }

  findById(id: string) {
    return from(this._reactionModel.findById(id));
  }

  addToTarget(creationDto: ReactionForCreationDto) {
    const { targetId, ownerId } = creationDto;
    return this.findByTargetAndOwner(targetId, ownerId).pipe(
      mergeMap((reaction) =>
        iif(
          () => !reaction,
          of(true).pipe(
            mergeMap(() => from(this._reactionModel.create(creationDto))),
          ),
          of(true).pipe(
            mergeMap(() =>
              this.update(reaction!.id, {
                type: creationDto.type,
              }),
            ),
          ),
        ),
      ),
    );
  }

  update(id: string, updateDto: ReactionForUpdateDto) {
    return this.findById(id).pipe(
      mergeMap((reaction) =>
        iif(
          () => reaction!.type !== updateDto.type,
          of(true).pipe(
            mergeMap(() => {
              reaction!.type = updateDto.type;
              return from(reaction!.save());
            }),
          ),
          of(reaction!),
        ),
      ),
    );
  }

  delete(id: string) {
    return from(this._reactionModel.deleteOne({ _id: id }));
  }
}
