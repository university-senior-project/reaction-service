import { ReactionDocument } from '../schemas/reaction.schema';

export interface GetReactionsForTargetResponse {
  reactions: Array<
    Pick<ReactionDocument, 'id' | 'targetId' | 'ownerId' | 'type'>
  >;
}
