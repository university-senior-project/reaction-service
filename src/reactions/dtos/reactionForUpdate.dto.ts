import { OmitType } from '@nestjs/mapped-types';
import { ReactionForCreationDto } from './reactionForCreation.dto';

export class ReactionForUpdateDto extends OmitType(ReactionForCreationDto, [
  'targetId',
  'ownerId',
] as const) {}
