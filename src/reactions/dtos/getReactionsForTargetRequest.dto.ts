import { IsString } from 'class-validator';

export class GetReactionsForTargetRequestDto {
  @IsString()
  targetId!: string;
}
