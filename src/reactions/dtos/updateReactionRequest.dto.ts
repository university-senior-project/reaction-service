import { IsString } from 'class-validator';

export class UpdateReactionRequestDto {
  @IsString()
  targetId!: string;

  @IsString()
  ownerId!: string;
}
