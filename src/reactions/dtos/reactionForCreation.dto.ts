import { IsEnum, IsString } from 'class-validator';
import { ReactionType } from '../enums/reactionType.enum';

export class ReactionForCreationDto {
  @IsString()
  targetId!: string;

  @IsEnum(ReactionType)
  type!: ReactionType;

  @IsString()
  ownerId!: string;
}
