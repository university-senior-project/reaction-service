import { UpdateReactionRequestDto } from './updateReactionRequest.dto';

export class DeleteReactionRequestDto extends UpdateReactionRequestDto {}
